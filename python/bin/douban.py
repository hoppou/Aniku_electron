# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author  : kyle
# Time    : 2020/12/8 18:51
# Function: reptiles for moegirl

import os
import sys

# 配置目录信息
DIR_BASE = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 根目录
sys.path.append(DIR_BASE)  # 添加根目录到搜索路径

if __name__ == '__main__':
    """start in there"""
    from src.douban_handle import run
    run()
