# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author  : kyle
# Time    : 2020/12/8 18:54
import os
import random
import json
from urllib3 import PoolManager
from bs4 import BeautifulSoup


class HTMLHandle(object):
    """HTML解析"""

    def __init__(self, http_data, name_flag=False):
        """
        实例化类
        :param http_data: 网页数据
        """
        self.user_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        self.__beautiful_soup = BeautifulSoup(http_data, 'html.parser')
        movie_scrip = self.__beautiful_soup.find(type='application/ld+json').string
        self.__movie_info = json.loads(movie_scrip, strict=False)
        self.name_flag = name_flag

    def __find_persion(self, person_dict):
        """
        找出所有作者
        :param person_dict: 名称字典
        :return:
        """
        name_list = []
        for person in person_dict:
            if self.name_flag:
                name_list.append(person['name'])
            else:
                name_list.append(person['name'].split()[0])
        return name_list

    def get_info(self):
        data_dict = dict()
        data_dict['name'] = self.__movie_info['name'].strip()
        data_dict['director'] = self.__find_persion(self.__movie_info['director'])
        data_dict['author'] = self.__find_persion(self.__movie_info['author'])
        data_dict['actor'] = self.__find_persion(self.__movie_info['actor'])
        data_dict['date'] = self.__movie_info['datePublished']
        data_dict['category'] = self.__movie_info['genre']
        data_dict['description'] = self.__movie_info['description']
        data_dict['rate'] = self.__movie_info['aggregateRating']['ratingValue']
        while True:
            num = str(random.randint(0, 9999999999))
            cover_url = '../db/cover/' + num + '.jpg'
            if not os.path.exists(cover_url):
                data_dict['cover'] = os.path.abspath(cover_url)
                break
        return data_dict

    def get_cover(self):
        url_cover = self.__movie_info['image']
        dl_manager = PoolManager(headers={'User-Agent': self.user_agent})
        image_data = dl_manager.request('GET', url_cover).data
        return image_data
