# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author  : kyle
# Time    : 2020/12/8 18:53
import json
from urllib import parse
from urllib3 import PoolManager


class DoubanMovie(object):
    """豆瓣电影信息提取"""

    def __init__(self):
        """
        实例化类
        """
        self.user_agent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
        self.search_url = 'https://movie.douban.com/j/subject_suggest?q='
        self.movie_url = 'https://movie.douban.com/subject/'
        self.http_manager = PoolManager(headers={'User-Agent': self.user_agent})

    def __get_subject(self, movie_name):
        """
        获取电影名称相关豆瓣subject id
        :param movie_name: 电影名称
        :return:
        """
        name_link = parse.quote(movie_name)
        http_get = self.http_manager.request('GET', self.search_url + name_link)
        http_data = json.loads(http_get.data.decode())
        if len(http_data) == 0:
            return None
        else:
            # TODO: 遍历title找出最相关
            return http_data[0]['id']

    def get_info(self, movie_name):
        """
        获取电影名称相关豆瓣网页
        :param movie_name: 电影名称
        :return:
        """
        name_id = self.__get_subject(movie_name)
        if not name_id:
            print('find no movie named: ', movie_name)
            return None
        http_get = self.http_manager.request('GET', self.movie_url + name_id + '/')
        http_data = http_get.data.decode()
        return http_data
