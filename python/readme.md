# Python服务说明

## 更新说明

2020-12-14 v0.1.0

python基本服务实现，可以生成db/title/movie目录下电影的详情信息

## 应用结构

./python/bin-----------python服务启动目录

./python/db/cover------电影封面存储位置

./python/db/info-------电影信息存储位置

./python/db/title------手机电影名称存储位置

./python/src-----------程序逻辑代码位置

./python/tools---------程序工具代码位置

## 数据结构说明

| 标题        | 说明                                              |
| ----------- | ------------------------------------------------- |
| key         | 电影搜索关键字，来源于./python/db/title/movie文件 |
| name        | 电影豆瓣名称                                      |
| director    | 监督或导演，"/"分割不同人名，以下类似             |
| author      | 作者                                              |
| actor       | 演员或声优                                        |
| date        | 上线日期                                          |
| category    | 标签                                              |
| description | 简介                                              |
| rate        | 豆瓣评分                                          |
| cover       | 封面存储绝对路径，运行时自动生成                  |
