# !/usr/bin/python3
# -*- coding:utf-8 -*-
# Author  : kyle
# Time    : 2020/12/13 21:01
import os
import json
from tools.http_handle import HTMLHandle
from tools.url_handle import DoubanMovie


def search_movie(movie_title):
    """
    查找单个电影
    :param movie_title: 电影名称
    :return:
    """
    douban = DoubanMovie()
    html_data = douban.get_info(movie_title)
    html = HTMLHandle(html_data)
    movie_info = html.get_info()
    movie_cover = html.get_cover()
    if not os.path.exists('../db/info/movie_info.csv'):
        with open('../db/info/movie_info.csv', 'w', encoding='utf-8') as f:
            f.write(','.join(['key', 'name', 'director', 'author', 'actor', 'date', 'category', 'description', 'rate',
                              'cover']) + '\n')
    info_list = [movie_title, movie_info['name']]
    info_list.append('/'.join(movie_info['director']))
    info_list.append('/'.join(movie_info['author']))
    info_list.append('/'.join(movie_info['actor']))
    info_list.append(movie_info['date'])
    info_list.append('/'.join(movie_info['category']))
    info_list.append(movie_info['description'])
    info_list.append(movie_info['rate'])
    info_list.append(movie_info['cover'])
    with open('../db/info/movie_info.csv', 'a', encoding='utf-8') as f:
        f.write(','.join(info_list) + '\n')
    with open(movie_info['cover'], 'wb') as f:
        f.write(movie_cover)


def run():
    """program start"""
    title_list = []
    with open('../db/title/movie', 'r', encoding='utf-8') as f:
        for line in f:
            title_list.append(line.strip())
    for title in title_list:
        search_movie(title)
